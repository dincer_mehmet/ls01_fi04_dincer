import java.util.concurrent.TimeUnit;

public class Leichtatleten {

	public static void main(String[] args) throws InterruptedException {

		double distanz = 1000;
		double geschwindigkeitA = 9.5;
		double geschwindigkeitB = 7;
		double gelaufenA = 0;
		double gelaufenB = 250;

		while (gelaufenA <= distanz && gelaufenB <= distanz) {

			gelaufenA += geschwindigkeitA;
			gelaufenB += geschwindigkeitB;

			System.out.println("\t\t  Sprinter A ist " + gelaufenA + "  m gerannt     		|     Sprinter B ist "
					+ gelaufenB + " m gerannt  ");

			if (gelaufenA >= distanz) {
				System.out.println("SprinterA hat gewonnen");
			} else if (gelaufenB >= distanz) {
				System.out.println("SprinterB hat gewonnen");
			}

			TimeUnit.SECONDS.sleep(1);
		}
	}
}
