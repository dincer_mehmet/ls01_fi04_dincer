
public class Temperaturtabelle {

	public static void main(String[] args) {
		String f = "Fahrenheit";
		String c = "Celcius";

		System.out.printf("%s\t|%10s\n", f, c);
		System.out.printf("------------------------------\n");
		System.out.printf("%-10s\t|%10s\n", -20, -28.9);
		System.out.printf("%-10s\t|%10s\n", -10, -23.3);
		System.out.printf("%-10s\t|%10s\n", +0, -17.8);
		System.out.printf("%-10s\t|%10s\n", +20, -6.7);
		System.out.printf("%-10s\t|%10s\n", +30, -1.1);

	}

}
